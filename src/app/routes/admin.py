from __future__ import annotations

from flask import Blueprint, session, redirect, url_for, render_template, flash, g

from src.app.controllers import OrderController
from src.app.routes.user import login_required

admin_area_blueprint = Blueprint("admin_area", __name__, url_prefix="/admin")
admin_product_info_blueprint = Blueprint("admin_product_info", __name__, url_prefix="/admin/products")
admin_orders_info_blueprint = Blueprint("admin_orders_info", __name__, url_prefix="/admin/orders")


@admin_area_blueprint.route("/")
@login_required
def admin_area():
    first_name = session["first_name"]
    if "admin" not in first_name:
        flash("You are not an admin", "warning")
        return redirect(url_for('root.root'))
    return render_template('adminArea.html', firstName=first_name, noOfItems=session["no_of_items"],
                           loggedIn=session["logged_in"])


@admin_product_info_blueprint.route("/")
@login_required
def admin_available_products_statistics():
    return redirect(url_for('root.root'))


@admin_orders_info_blueprint.route("/")
def admin_orders_statistics():
    ses = g.session
    orders = OrderController(ses).get_orders_with_product_info()

    dynamic_data_table = get_dynamic_table(orders)

    return render_template("adminOrderStats.html", cloumnHeaders=dynamic_data_table[0],
                           tableValues=dynamic_data_table[1:],
                           firstName=session["first_name"], noOfItems=session["no_of_items"],
                           loggedIn=session["logged_in"])


def get_dynamic_table(orders: list[list[str | int]]) -> list[list[str | int]]:
    """
    orders besteht aus einer Liste aus Listen. Eine Liste besteht jeweils aus einer order_item_id (int), einer order_id (int) und
    dem product_name (str).

    :param orders: list[list[str | int]] Rohe Daten, aus welchen eine Statistik erstellt wird
    :return: das 2d Array mit der Statistik mit den product_name als Spaltennamen und der order_id als Reihennamen
    """

    # TODO: Code zum fixen des Tests verstehen und Codequalität verbessern
    if not orders:
        return orders

    all_product_names: list[str] = []
    all_orders: dict[int, dict[str, int]] = {}

    for order_item_id, order_id, product_name in orders:
        # Einzigartige Produktnamen für Spaltenüberschriften sammeln
        if product_name not in all_product_names:
            all_product_names.append(product_name)

        # Anzahl der jeweiligen Produktnamen in der Bestellung pro Bestell-ID zählen
        if order_id not in all_orders:
            all_orders[order_id] = {}

        if product_name in all_orders[order_id]:
            all_orders[order_id][product_name] += 1
        else:
            all_orders[order_id][product_name] = 1

    # Spaltenüberschriften lexikographisch sortieren
    all_product_names = sorted(all_product_names)
    all_column_headers: list[str | int] = ["Order ID"] + all_product_names + ["Total"]

    dynamic_table: list[list[str | int]] = [all_column_headers]

    # 2D-Array mit Werten füllen, wobei die Spaltenüberschriften Produktnamen und die Zeilenüberschriften Bestell-IDs sind
    for row_order_id in sorted(all_orders):
        row: list[str | int] = [row_order_id]
        row_total = 0
        for product_name in all_product_names:
            product_count = all_orders[row_order_id].get(product_name, 0)
            row.append(product_count)
            row_total += product_count
        row.append(row_total)
        dynamic_table.append(row)

    # Gesamtsummenreihe erstellen, die die Gesamtsumme für jede Spalte enthält
    total_row: list[str | int] = ["Total"]
    for column in list(zip(*dynamic_table[1:]))[1:]:
        total_row.append(sum(column))

    dynamic_table.append(total_row)

    return dynamic_table if len(dynamic_table) > 1 else []


def calculate_row_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    return dynamic_table


def calculate_column_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    pass
